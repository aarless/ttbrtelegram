#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tweepy
import telegram
import json
from configparser import ConfigParser

#Credenciais de aplicação do Twitter
CONSUMER_KEY = ''
CONSUMER_SECRET = ''
ACCESS_KEY = ''
ACCESS_SECRET = ''

#Autenticação
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)
api = tweepy.API(auth)

#Trending Topics
trends1 = api.trends_place(23424768)

#Parsear JSON
data = trends1[0]
trends = data['trends']
names = [trend['name'] for trend in trends]
tops = '\n'.join(names)

#Ler arquivo de configuração do bot do Telegram
config = ConfigParser()
config.read_file(open('config.ini'))
token = config['KEYS']['token']
chat_id = config['CHAT']['id']

#Enviar mensagem para o Telegram
bot = telegram.Bot(token=token)
bot.send_message(chat_id=chat_id, text=tops)